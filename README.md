# DAFS

This document describes the interfaces for the DAF (Dynamic anonymous bicycle register). This initial version is focussed on registering bicycles with framenumber that are check-in / out of bicycle depots.

## Overview

Data communication is by https requests with an api-key specified for authentication. For now we specified one endpoint to exchange data. When a request failes due to unavailability of the server the request should be repeated later.

### /event

##### event object

| Field               | Type                | Required               | Description                                                  |
| ------------------- | ------------------- | ---------------------- | ------------------------------------------------------------ |
| event_type          | enum                | yes                    | In this initial version "check_in_depot" or "check_out_depot" |
| extra_information   |                     | no                     |                                                              |
| timestamp           | ISO8601 timestamp   | yes                    | UTC timestamp example 2019-09-22T11:14:00Z                   |
| bike                | bike                | yes                    | A bike object as described below.                            |
| depot_details       | depot_details       | conditionally required | Should be provided when event_type is "check_in_depot" or "check_out_depot" |
| enforcement_details | enforcement_details | conditionally required | Should be provided when event_type is "check_in_depot", gives details of enforcement |



##### bike object

| Field           | Type   | Required               | Description                                                    |
| ------------    | ------ | --------               | ------------------------------------------------------------   |
| brand           | string | yes                    | e.g. swapfiets, gazelle                                        |
| frame_number    | string | conditionally required | One of frame_number, chip_number and license_plate is required |
| chip_number     | string | conditionally required | Number of chip on the bicycle                                  |
| license_plate   | string | conditionally required | License plate of scooter / speed pedelec                       |
| color           | string | no                     | textual description of color                                   |
| description     | string | no                     | Free text description of bicycle e.g. "Blauwe stadsfiets met fietstassen" |

##### depot_details object

| Field             | Type   | Required | Description                                                  |
| ----------------- | ------ | -------- | ------------------------------------------------------------ |
| depot_id          | string | yes      | Unique id of depot, an internal id of a data supplier can be used when this is id is prepended by a prefix, for example "pf:<internal_id>" |
| municipality_code | string | yes      | municipality code where depot is located as provided by CBS e.g. *GM0344* |
| name              | string | no       |                                                              |
| street            | string | no       |                                                              |
| postal_code       | string | no       |                                                              |
| city              | string | yes      | Name of city                                                 |
| phone_number      | string | no       |                                                              |
| email             | string | no       |                                                              |
| instruction       | string | no       |                                                              |

##### enforcement_details object

| Field                | Type              | Required | Description                                                  |
| -------------------- | ----------------- | -------- | ------------------------------------------------------------ |
| city                 | string            | yes      | city name of enforcement                                     |
| municipality_code    | string            | yes      | municipality_code of encorcement                             |
| location             | location (wsg84)  | no       |                                                              |
| location_description | string            | no       | Tekstual description of location "Stationsstalling Jaarbeurszijde" |
| free_text_reason     | string            | no       | Reason for enforcement.                                      |
| timestamp            | ISO8601 timestamp | no       | Timestamp of enforcement                                     |

#### location
| Field                | Type              | Required | Description                                                  |
| -------------------- | ----------------- | -------- | ------------------------------------------------------------ |
| latitude             | number            | yes      | 52.0                                                          |
| longitude            | number            | yes      | 5.0                                                         |



##### Example:

```json
{
    "event_type": "check_in_depot",
    "extra_information": "Optionele extra informatie over waarom fiets bijvoorbeeld is meegenomen.",
    "timestamp": "2019-09-22T11:14:00Z",
    "bike": {
    	"frame_number": "12277588",
      "chip_number": "54513216",
      "license_plate": "44AGR4",
    	"brand": "Swapfiets",
      "color": "blue",
      "description": "Blauwe swapfiets met fietstas."
    },
  "depot_details": {
      "depot_id": "pf:42",
      "municipality_code": "GM0344",
      "name": "Kanaalweg",
      "street": "Kanaalweg 50",
      "postal_code": "3526KM",
      "phone_number": "14 030",
      "email": "fietsdepot@utrecht.nl",
      "instruction": "Haal de fiets op bij de kanaalweg",
      "city": "Utrecht"
    },
  "enforcement_details": {
      "city": "Houten",
      "municipality_code": "GM0321",
      "location_description": "Houten stations, fietsenstalling.",
      "location" : {
        "latitude": 52.0,
        "longitude": 5.0
      },
      "free_text_reason": "Langer dan 28 dagen geparkeerd.",
      "timestamp": "2019-09-22T10:54:05Z"
    }
}
```

